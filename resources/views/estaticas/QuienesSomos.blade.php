<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alquiler De Pisos</title>

    <!-- estilos principales -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/estilo_estaticas.css">

    <!-- Custom CSS -->
   		<link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="plugins/cropit/cropit.css">
        <link rel="stylesheet" href="css/CSSScript.css">
 

</head>

<body>
    <!-- Header -->

   <nav class="navbar navbar-static-top">
        <div class="container">
        <div class="navbar-header">
        <div class="navbar-brand-custom" >
        <i class="fa fa-envelope-o" aria-hidden="true"></i>
        info@alquilerdepisos</div>
        <div class="navbar-brand-custom" >
        <i class="fa fa-phone" aria-hidden="true"></i>
      (+57) 311 736 4378</div>
        </div>
        </div>
    </nav>

<nav class="navbar navbar-menu-top" >
    <div class="container">
    <div class="navbar-header">
      <a href="http://alquileres-pisos.com" class="navbar-brand-custom" ><img src="http://alquileres-pisos.com/img/logo.png" ></a>
      <a class="navbar-brand-custom" href="http://alquileres-pisos.com/publication-list?params=tipo%3Darrendamiento" >Anuncios de arrendamiento</a>
      <a class="navbar-brand-custom" href="http://alquileres-pisos.com/publication-list?params=tipo%3Dventa" >Anuncios de venta</a>
      <a class="navbar-brand-custom" href="http://alquileres-pisos.com/login">Ingresar</a>
      <a class="navbar-brand-custom" href="http://alquileres-pisos.com/register">Registrar</a>
    </div>
    </div>
</nav>

    <!-- Contenido -->
    <div class="container">

        <hr class="featurette-divider">

        <!-- First Featurette -->
        <div class="featurette" id="about">
            <img class="featurette-image img-circle img-responsive pull-right" src="img/casa2.png">
            <h2 class="featurette-heading">Una Empresa Responsable
            </h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">

        <!-- Second Featurette -->
        <div class="featurette" id="services">
            <img class="featurette-image img-circle img-responsive pull-left" src="img/casa2.png">
            <h2 class="featurette-heading">Dedicados al Consumidor
            </h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">

        <!-- Third Featurette -->
        <div class="featurette" id="contact">
            <img class="featurette-image img-circle img-responsive pull-right" src="img/casa2.png">
            <h2 class="featurette-heading">Calidad Asegurada
            </h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">



</div>
    <!--Footer -->

<footer>
      <div class="container">
        <div class="col-sm-3" >
          <div class="links_footer">
            <h5>Oferta de Alquiler en Colombia</h5>
            <ul>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DAraba%2F%C3%81lava">Ofertas de Araba/Álava </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DAlbacete">Ofertas de Albacete </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DAlicante%2FAlacant">Ofertas de Alicante/Alacant </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DAlmer%C3%ADa">Ofertas de Almería </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3D%C3%81vila">Ofertas de Ávila </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DBadajoz">Ofertas de Badajoz </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DBalears%2C+Illes">Ofertas de Balears, Illes </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DBarcelona">Ofertas de Barcelona </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DBurgos">Ofertas de Burgos </a></li>
                                                        <li><a href="http://alquileres-pisos.com/publication-list?params=ubicacion%3DC%C3%A1ceres">Ofertas de Cáceres </a></li>
                            </ul>
          </div>
        </div>


        <div class="col-sm-3" >
          <div class="links_footer">
            <a href="{{ route('estaticas.Somos') }}" style="text-decoration: none; color: #ffc23c;"><h5><b>¿Quienes Somos?</b></h5></a>
            <ul>
              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc erat nunc, luctus ac ullamcorper non, viverra eget mauris. Aenean convallis dui eu nisl vestibulum dictum. Suspendisse potenti. Nam iaculis augue ante, non euismod velit dictum et. In sit amet ipsum venenatis, feugiat tortor vel, rhoncus eros.</li>
            </ul>
          </div>
        </div>
        <div class="col-sm-3" >
          <div class="links_footer">
            <h5>Oferta de Anuncios</h5>
            <ul>
              <li><h5>Venta de Nuevo y Usados</h5></li>
              <li><a href="http://alquileres-pisos.com/publication-list?tipo=Venta&amp;condicion=Nuevo">Proyectos Nuevos</a></li>
              <li><a href="http://alquileres-pisos.com/publication-list?tipo=Venta&amp;condicion=Usado">Proyectos Usados</a></li>
              <li><a href="http://alquileres-pisos.com/publication-list?tipo=Arriendo&amp;grado=Ofertas">Ofertas en Arriendo</a></li>
              <li><a href="http://alquileres-pisos.com/publication-list?tipo=Vacacionales&amp;grado=Ofertas">Ofertas Vacacionales</a></li>
            </ul>
            <h5>Nuestros clientes</h5>
            <ul>
              <li><a href="http://alquileres-pisos.com/inmobiliarias" style="text-decoration: none; color: width;">Inmobiliarias</a></li>
              <li><a href="http://alquileres-pisos.com/constructoras" style="text-decoration: none; color: width;">Constructoras</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-3" >
          <div class="links_footer">
            <h5>Nuestros Servicios</h5>
            <ul>
              <li><a href="http://alquileres-pisos.com/gestion-personal" style="text-decoration: none; color: width;">Gestion Personal</a></li>
              <li><a href="http://alquileres-pisos.com/gestion-empresarial" style="text-decoration: none; color: width;">Gestion Empresarial</a></li>
              <li><a href="http://alquileres-pisos.com/publications/create" style="text-decoration: none; color: width;">Publicar inmueble</a></li>
            </ul>
            <h5>Servicio al Cliente</h5>
            <ul>
              <li><a href="{{ route('estaticas.contacto') }}" style="text-decoration: none; color: width;">Conactenos</a></li>
              <li><a href="{{ route('estaticas.preguntas') }}" style="text-decoration: none; color: width;">Preguntas Frecuentes</a></li>
              <li><a href="{{ route('estaticas.recomendaciones') }}" style="text-decoration: none; color: width;">Recomendaciones</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 marT15">
          <div class="pull-left">
            <img src="http://alquileres-pisos.com/img/logo-negativo.png">
          </div>
          <div class="text-center social-foot">
            <a href="https://www.facebook.com/Alquilerdepisosycasas-1576487502423886/"><img class="ico-20px marL13 marR13" src="http://alquileres-pisos.com/img/logo-fb.png"></a>
            <a href="https://twitter.com/alquipisosycasa"><img class="ico-20px marL13 marR13" src="http://alquileres-pisos.com/img/logo-twit.png"></a>

            <a href="https://plus.google.com/u/3/109712210960165396538"><img class="ico-20px marL13 marR13" src="http://alquileres-pisos.com/img/logo-gplus.png"></a>
            <a href="https://mail.google.com/"><img class="ico-20px marL13 marR13" src="http://alquileres-pisos.com/img/logo-mail.png"></a>

          </div>

        </div>
      </div>
<center> <small style="color:white;">&copy; Copyright 2017 - Alquiler de Pisos</small></center>

    </footer>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
