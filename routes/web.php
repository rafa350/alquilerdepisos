<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::name('estaticas.Somos')->get('QuienesSomos','QuienesSomosController@QuienesSomos');

Route::name('estaticas.contacto')->get('Contacto','QuienesSomosController@contacto');

Route::name('estaticas.preguntas')->get('Preguntas_Frecuentes','QuienesSomosController@preguntas');

Route::name('estaticas.recomendaciones')->get('Recomendaciones','QuienesSomosController@recomendaciones');


