<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuienesSomosController extends Controller
{
    //
    public function QuienesSomos()
  {
    return view('estaticas.QuienesSomos');
  }

 public function contacto()
  {
    return view('estaticas.contacto');
  }

 public function preguntas()
  {
    return view('estaticas.preguntas');
  }

public function recomendaciones()
  {
    return view('estaticas.recomendaciones');
  }
}
